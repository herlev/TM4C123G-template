.PHONY: flash clean init
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))

DEVICE_PATH=$(mkfile_dir)TM4C123G
CC=arm-none-eabi-gcc
CXX=arm-none-eabi-g++
INCLUDE+=-I$(mkfile_dir)TM4C123G/ -I./inc/
MACH=cortex-m4
OPTIMIZATION=-Og
DEBUG=-g3

SRCS+=$(shell fd -e c . src/)
CPP_FILES=$(wildcard src/*.cpp)
STARTUP_OBJ=$(DEVICE_PATH)/startup.o
SYSCALLS_OBJ=$(DEVICE_PATH)/syscalls.o

COMMON_FLAGS=-mthumb -mcpu=$(MACH) -mfloat-abi=hard -mfpu=fpv4-sp-d16
CFLAGS= -c $(INCLUDE) $(COMMON_FLAGS) -Wall $(DEBUG) $(OPTIMIZATION)

CXXFLAGS = -fno-threadsafe-statics
CXXFLAGS += -fno-rtti
CXXFLAGS += -fno-exceptions
CXXFLAGS += -fno-unwind-tables
CXXFLAGS += -Wno-register

LDFLAGS= $(COMMON_FLAGS) --specs=nano.specs -T $(DEVICE_PATH)/linker.ld -Wl,-Map=bin/final.map
DEP=$(SRCS:.c=.d)
OBJECTS=$(STARTUP_OBJ) $(SYSCALLS_OBJ) $(SRCS:.c=.o) $(CPP_FILES:.cpp=.o)

all: bin/main.bin

%.o: %.c
	$(CC) -std=gnu18 $(CFLAGS) -o $@ $<

%.o: %.cpp
	$(CXX) $(CFLAGS) $(CXXFLAGS) -o $@ $<

-include $(DEP)
%.d: %.c
	@$(CC) $(CFLAGS) $< -MM -MT $(@:.d=.o) >$@

bin/main.elf: $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

%.bin: %.elf
	arm-none-eabi-objcopy $^ $@ -O binary

flash: bin/main.bin
	lm4flash $^

test:
	echo $(SRCS)

init:
	mkdir -p bin src inc
	cp $(mkfile_dir)example/main.c src/main.c
	cp -r $(mkfile_dir)editorconfig/.vscode .
	cp -r $(mkfile_dir)editorconfig/.ccls .

clean:
	rm -rf $(shell fd -I -e o -e bin -e elf -e map -e d)

load:
	openocd -s /usr/share/openocd/scripts -f board/ek-tm4c123gxl.cfg

debug:
	arm-none-eabi-gdb bin/main.elf -ex "target remote :3333" -ex "load"

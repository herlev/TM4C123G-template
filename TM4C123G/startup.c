#include <stdint.h>

#define SRAM_START  0x20000000U
#define SRAM_SIZE   0x8000U
#define SRAM_END    ((SRAM_START) + (SRAM_SIZE))

#define STACK_START   SRAM_END

extern uint32_t _etext;
extern uint32_t _sdata;
extern uint32_t _edata;
extern uint32_t _la_data;

extern uint32_t _sbss;
extern uint32_t _ebss;

int main(void);

void __libc_init_array(void);

void Default_Handler(void);
void Reset_Handler(void);

void NMI_Handler              (void) __attribute__ ((weak, alias("Default_Handler")));
void HardFault_Handler        (void) __attribute__ ((weak, alias("Default_Handler")));
void MemManage_Handler        (void) __attribute__ ((weak, alias("Default_Handler")));
void BusFault_Handler         (void) __attribute__ ((weak, alias("Default_Handler")));
void UsageFault_Handler       (void) __attribute__ ((weak, alias("Default_Handler")));
void SVC_Handler              (void) __attribute__ ((weak, alias("Default_Handler")));
void DebugMon_Handler         (void) __attribute__ ((weak, alias("Default_Handler")));
void PendSV_Handler           (void) __attribute__ ((weak, alias("Default_Handler")));
void SysTick_Handler          (void) __attribute__ ((weak, alias("Default_Handler")));
void GPIOA_Handler          (void) __attribute__ ((weak, alias("Default_Handler")));
void GPIOB_Handler          (void) __attribute__ ((weak, alias("Default_Handler")));
void GPIOC_Handler          (void) __attribute__ ((weak, alias("Default_Handler")));
void GPIOD_Handler          (void) __attribute__ ((weak, alias("Default_Handler")));
void GPIOE_Handler          (void) __attribute__ ((weak, alias("Default_Handler")));

uint32_t vectors[] __attribute__((section(".isr_vector"))) = {
    STACK_START,
    (uint32_t)Reset_Handler,
    (uint32_t)NMI_Handler,
    (uint32_t)HardFault_Handler,
    (uint32_t)MemManage_Handler,
    (uint32_t)BusFault_Handler,
    (uint32_t)UsageFault_Handler,
    0,
    0,
    0,
    0,
    (uint32_t)SVC_Handler,
    (uint32_t)DebugMon_Handler,
    0,
    (uint32_t)PendSV_Handler,
    (uint32_t)SysTick_Handler,
    (uint32_t)GPIOA_Handler,
    (uint32_t)GPIOB_Handler,
    (uint32_t)GPIOC_Handler,
    (uint32_t)GPIOD_Handler,
    (uint32_t)GPIOE_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    0,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    0,
    0,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    0,
    0,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    0,
    0,
    0,
    0,
    0,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    0,
    0,
    0,
    0,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
    (uint32_t)Default_Handler,
};

#include "tm4c123gh6pm.h"
void Default_Handler(void) {
  GPIO_PORTF_DATA_R |= 0x02;
  while (1)
    ;
}

void Reset_Handler(void) {
  // copy .data section to SRAM
  uint32_t size = (uint32_t)&_edata - (uint32_t)&_sdata;

  uint8_t *pDst = (uint8_t *)&_sdata;   // sram
  uint8_t *pSrc = (uint8_t *)&_la_data; // flash

  for (uint32_t i = 0; i < size; i++) {
    *pDst++ = *pSrc++;
  }

  // Init. the .bss section to zero in SRAM
  size = (uint32_t)&_ebss - (uint32_t)&_sbss;
  pDst = (uint8_t *)&_sbss;
  for (uint32_t i = 0; i < size; i++) {
    *pDst++ = 0;
  }

  // Enable FPU
  NVIC_CPAC_R |= NVIC_CPAC_CP11_FULL | NVIC_CPAC_CP10_FULL;
  __libc_init_array();

  main();
}

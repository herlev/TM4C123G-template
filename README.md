# TM4C123G-template

Template to be used with the Tiva C Series TM4C123G Launchpad Evaluation Kit

## Get started

```bash
git clone https://gitlab.com/herlev/TM4C123G-template ~/TM4C123G-template
```

### Create project
```bash
mkdir project && cd project
echo "include ~/TM4C123G-template/Makefile" > makefile
make init
make flash
```
